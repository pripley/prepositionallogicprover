package LogicTestSupport;

import junit.framework.*;
import junit.extensions.*;
import WFFSupport.*;

public class LogicTestSuite {
  public static Test suite() {
    TestSuite suite = new TestSuite();

    suite.addTest( new TestMP("Test MP") { 
      protected void runTest() { testMP(); } 
    } );
	suite.addTest( new TestImplicationIntroduction("Test Implication Introduction") { 
      protected void runTest() { testImplicationIntroduction(); } 
    } );
	suite.addTest( new TestReductio("Test Reductio") { 
      protected void runTest() { testReductio(); } 
    } );
	suite.addTest( new TestBiconditionalElimination("Test Biconditional Elimination") { 
      protected void runTest() { testBiconditionalElimination(); } 
    } );
	suite.addTest( new TestBiconditionalIntroduction("Test Biconditional Introduction") { 
      protected void runTest() { testBiconditionalIntroduction(); } 
    } );
    return suite;
  }

  public static void main (String[] args) {
    junit.textui.TestRunner.run (suite());
  }
}