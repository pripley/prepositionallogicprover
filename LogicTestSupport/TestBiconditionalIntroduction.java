package LogicTestSupport;

import junit.framework.*;
import junit.extensions.*;
import java.util.*;
import WFFSupport.*;

public class TestBiconditionalIntroduction extends TestCase {

  private WFF m_Want1;
  private WFF m_Want2;
  private WFF m_Want3;
  private WFF m_Want4;
  private WFF m_Have1;
  private WFF m_Have2;
  private WFF m_Have3;
  private WFF m_Have4;
  private ArrayList<WFF> m_DownScope;
  private ProofGenerator m_ProofGenerator;

  public TestBiconditionalIntroduction(String testName) {
    System.out.println(testName);
  }

  protected void setUp() {

    try {
      WFFFactory fact = new WFFFactory();
	  m_Want1 = fact.makeWFF("p#q");
	  m_Want2 = fact.makeWFF("z#y");
	  //m_Want3 = fact.makeWFF("q>p");
	  //m_Want4 = fact.makeWFF("i>(j>(~k))");
      m_Have1 = fact.makeWFF("p>q");
      m_Have2 = fact.makeWFF("q>p");
	  m_Have3 = fact.makeWFF("z");
	  m_Have4 = fact.makeWFF("y");
	  m_DownScope = new ArrayList<WFF>();
	  m_DownScope.add(m_Have1);
	  m_DownScope.add(m_Have2);
	  m_DownScope.add(m_Have3);
	  m_DownScope.add(m_Have4);	  
	  m_ProofGenerator = new ProofGenerator();
    
	} catch(BadWFFException e) {
      System.out.println("The formula was not well-formed.");
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void testBiconditionalIntroduction() {
    Assert.assertEquals(true, m_ProofGenerator.newProof(m_Want1, m_DownScope));
	Assert.assertEquals(true, m_ProofGenerator.newProof(m_Want2, m_DownScope));
    //Assert.assertEquals(true, m_ProofGenerator.newProof(m_Want3, m_DownScope));
    //Assert.assertEquals(true, m_ProofGenerator.newProof(m_Want4, m_DownScope));
  }
  
}