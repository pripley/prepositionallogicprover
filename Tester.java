import java.io.*;
import java.util.*;
import WFFSupport.*;

public class Tester {
  public static void main(String arg[]) {
    WFFFactory fact = new WFFFactory();
    String want = "~p";
    //have ~p>q want ~q>~p messes up 
    String have1 = "p>q";
    String have3 = "p>(~q)";
  //  String have2 = "k";
 //   System.out.println("Have:"+have1+" and "+have2);
    try {
/*
WFF w0 = new WFF("q");
WFF w1 = new WFF("p");
WFF w2 = new WFF("r");
WFF w3 = new WFF("d");
LogicalParticle lp = new HorseShoe();
LogicalParticle lp1 = new HorseShoe();
LogicalParticle lp2 = new HorseShoe();
WFF w4 = new WFF(lp, w0, w3);
WFF w5 = new WFF(lp1, w1, w4);
WFF w = new WFF(lp2, w5, w2);
*/
      WFF w = fact.makeWFF(want);
      WFF w1 = fact.makeWFF(have1);
//      WFF w2 = fact.makeWFF(have2);
      WFF w3 = fact.makeWFF(have3);

      ProofGenerator g = new ProofGenerator();
      

      ArrayList downScope = new ArrayList();
      downScope.add(w1);
//      downScope.add(w2);
      downScope.add(w3);

      if (g.newProof(w, downScope)) { System.out.println("success"); } else { System.out.println("failure"); }

    } catch(BadWFFException e) {
      System.out.println("The formula was not well-formed.");
    }
  }
}