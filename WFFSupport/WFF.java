package WFFSupport;

public class WFF {
  private LogicalParticle m_LP;
  private WFF m_LeftWFF;
  private WFF m_RightWFF;
  private boolean m_IsLetter;
  private String m_Value = "";

  /**
  * Constructer for simple WFFs
  */
  public WFF(String s) {
    m_LP = null;
    m_LeftWFF = null;
    m_RightWFF = null;
    m_IsLetter = true;
    m_Value = s;
  }
  
  /**
  * Constructer for complex WFFs
  */
  public WFF(LogicalParticle lp, WFF leftChild, WFF rightChild) {
    m_LP = lp;
    m_LeftWFF = leftChild;
    m_RightWFF = rightChild;
    m_IsLetter = false;
 
  }

  public LogicalParticle getMainLogicalParticle() {
    return m_LP;
  }
  
  public boolean isLetter() {
    return m_IsLetter;
  }

  public String getLetter() {
    return m_Value;
  }

  public WFF getLeftWFF() {
    return m_LeftWFF;
  }
  public WFF getRightWFF() {
    return m_RightWFF;
  }

  protected void setLeftWFF(WFF w) {
    m_LeftWFF = w;
  }

  protected void setRightWFF(WFF w) {
    m_RightWFF = w;
  }
  
  /**
  * Tests if a WFF formula is the same as this one
  */
  public boolean equals(WFF w) {
    //are both WFFs sentenceLetters?
   
    if ((w.isLetter()) && (isLetter())) {
      if (w.getLetter().equals(getLetter())) {
        return true;
      }
      else {
        return false;
      }
    }
    //one is a sentence letter the other is not
    if (((w.isLetter()) && (!isLetter())) || ((!w.isLetter()) && (isLetter()))) {
      return false;
    }
    //both WFFs are complex
    else {
      if ((getMainLogicalParticle().equals(w.getMainLogicalParticle())) && (w.getRightWFF().equals(getRightWFF()))) {
        if  ((w.getLeftWFF() == null) && (getLeftWFF() == null)) {
          return true;
        }
        if  ((w.getLeftWFF() == null) || (getLeftWFF() == null)) {
          return false;
        }
        if  (w.getLeftWFF().equals(getLeftWFF())) {
          return true;
        }
        return false;
      }
      else {
        return false;
      }
    }
  }
  
  /**
  * Checks if this WFF contains the inputted WFF 
  */
  public boolean contains(WFF w) {
    if (equals(w)) { return true; }
    if (getLeftWFF() != null) { if(getLeftWFF().contains(w)) { return true; } }
    if (getRightWFF() != null) { if (getRightWFF().contains(w)) { return true; } }
    return false;
  }
  
  /**
  * Prints out the WFF to the command line
  */
  public void printFormula() {
    if (this.isLetter()) {
      System.out.println(m_Value);
      return;
    }
    else {
      getMainLogicalParticle().printParticle();
      if (getLeftWFF() != null) { getLeftWFF().printFormula(); }
      if (getRightWFF() != null) { getRightWFF().printFormula(); }
    }
  }
  
  /**
  * same as printFormula(), but returns a String instead
  */
  public String getFormula() {
    if (this.isLetter()) {
      return m_Value;
    }
    else {
      String s = "";
      if (getLeftWFF() != null) { 
        if (getLeftWFF().isLetter()) {
          s = s + getLeftWFF().getFormula();
        }
        else {
          s = s + "(" + getLeftWFF().getFormula() + ")";
        }
      }     
      s = s + getMainLogicalParticle().getParticle();
      if (getRightWFF() != null) { 
        if (getRightWFF().isLetter()) {
          s = s + getRightWFF().getFormula();
        }
        else {
          s = s + "(" + getRightWFF().getFormula() + ")";
        }
      }
      return s;
    }
  }
}