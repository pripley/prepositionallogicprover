package WFFSupport.proofPrinterSupport;

import WFFSupport.*;

public class GivenNode implements ProofNode {
  private WFF m_WFF;
  private int m_LineNum;

  public GivenNode(int lineNum, WFF w) {
    m_WFF = w;
    m_LineNum = lineNum;
  }
  
  public WFF getWFF() {
    return m_WFF;
  }
  public String getRule() {
    return "given";
  }
  public String getReason() {
    return getRule();
  }
  public int getLineNum() {
    return m_LineNum;
  }
  public boolean isHyp() {
    return false;
  }
}