package WFFSupport.proofPrinterSupport;

import WFFSupport.*;

public class CENode implements ProofNode {
  private WFF m_WFF;
  private int m_LineNum;
  private int m_Line;
  
  public CENode(int lineNum, WFF w, int line) {
    m_WFF = w;
    m_Line = line;
    m_LineNum = lineNum;
  }
  
  public WFF getWFF() {
    return m_WFF;
  }
  public String getRule() {
    return "CE";
  }
  public String getReason() {
    return m_Line + "," + "CE";
  }
  public int getLineNum() {
    return m_LineNum;
  }
  public boolean isHyp() {
    return false;
  }

}