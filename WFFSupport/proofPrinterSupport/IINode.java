package WFFSupport.proofPrinterSupport;

import WFFSupport.*;

public class IINode implements ProofNode {
  private WFF m_WFF;
  private int m_LineNum;
  private int m_LineNumFrom;
  private int m_LineNumTo;
  
  public IINode(int lineNum, WFF w, int lineFrom, int lineTo) {
    m_WFF = w;
    m_LineNumFrom = lineFrom;
    m_LineNumTo = lineTo;
    m_LineNum = lineNum;
  }
  
  public WFF getWFF() {
    return m_WFF;
  }
  public String getRule() {
    return "II";
  }
  public String getReason() {
    return m_LineNumFrom + "-" + m_LineNumTo + "," + "II";
  }
  public int getLineNum() {
    return m_LineNum;
  }
  public boolean isHyp() {
    return false;
  }

}