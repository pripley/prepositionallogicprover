package WFFSupport.proofPrinterSupport;

import WFFSupport.*;

public class CINode implements ProofNode {
  private WFF m_WFF;
  private int m_LineNum;
  private int m_Line1;
  private int m_Line2;
  
  public CINode(int lineNum, WFF w, int line1, int line2) {
    m_WFF = w;
    m_Line1 = line1;
    m_Line2 = line2;
    m_LineNum = lineNum;
  }
  
  public WFF getWFF() {
    return m_WFF;
  }
  public String getRule() {
    return "CI";
  }
  public String getReason() {
    return m_Line1 + "," + m_Line2 + "," + "CI";
  }
  public int getLineNum() {
    return m_LineNum;
  }
  public boolean isHyp() {
    return false;
  }

}