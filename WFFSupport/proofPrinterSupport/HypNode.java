package WFFSupport.proofPrinterSupport;

import java.util.*;
import WFFSupport.*;

public class HypNode implements ProofNode {
  private WFF m_WFF;
  private ArrayList m_OnScope;
  private int m_TotalLines = -1;
  private int m_LineNum;
  
  public HypNode(int lineNum, WFF w, ArrayList onScope) {
    m_WFF = w;
    m_OnScope = onScope;
    m_LineNum = lineNum;
  }
    
  public ArrayList getOnScope() {
    return m_OnScope;
  }
  
  public WFF getWFF() {
    return m_WFF;
  }
  public String getRule() {
    return "hyp";
  }
  public int getLineNum() {
    return m_LineNum;
  }
  public boolean isHyp() {
    return true;
  }

  public String getReason() {
    return getRule();
  }
  public int getTotalLines() {
    if (m_TotalLines != -1) {
      return m_TotalLines;
    }
    else {
      int i = calculateTL(m_OnScope);
      m_TotalLines = i;
      return i;
    }
  }

  private int calculateTL(ArrayList a) {
    int j = 0;
    Iterator i = a.iterator();
    ProofNode q;
    while (i.hasNext()) {
      q = (ProofNode)i.next();
      j++;
      if (q.getRule().equals("hyp")) {
        HypNode n = (HypNode)q;
        j = j + calculateTL(n.getOnScope());
      } 
    }
    return j;
  }
}