package WFFSupport.proofPrinterSupport;

import WFFSupport.*;

public class DINode implements ProofNode {
  private WFF m_WFF;
  private int m_LineNum;
  private int m_Line1;
  
  public DINode(int lineNum, WFF w, int line1) {
    m_WFF = w;
    m_Line1 = line1;
    m_LineNum = lineNum;
  }
  
  public WFF getWFF() {
    return m_WFF;
  }
  public String getRule() {
    return "DI";
  }
  public String getReason() {
    return m_Line1 + ", " + "DI";
  }
  public int getLineNum() {
    return m_LineNum;
  }
  public boolean isHyp() {
    return false;
  }

}