package WFFSupport.proofPrinterSupport;

import WFFSupport.*;

public class MPNode implements ProofNode {
  private WFF m_WFF;
  private int m_Line1;
  private int m_Line2;
  private int m_LineNum;
  
  public MPNode(int lineNum, WFF w, int line1, int line2) {
    m_WFF = w;
    m_Line1 = line1;
    m_Line2 = line2;
    m_LineNum = lineNum;
  }
  
  public WFF getWFF() {
    return m_WFF;
  }
  public String getRule() {
    return "MP";
  }
  public String getReason() {
    return m_Line1 + "," + m_Line2 + "," + getRule();
  }
  public int getLineNum() {
    return m_LineNum;
  }
  public boolean isHyp() {
    return false;
  }

}