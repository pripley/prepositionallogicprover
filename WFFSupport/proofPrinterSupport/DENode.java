package WFFSupport.proofPrinterSupport;

import WFFSupport.*;

public class DENode implements ProofNode {
  private WFF m_WFF;
  private int m_LineNum;
  private int m_DJLine;
  private int m_LineTo1;
  private int m_LineTo2;
  private int m_LineFrom1;
  private int m_LineFrom2;
  
  public DENode(int lineNum, WFF w, int disLine, int lineTo1, int lineFrom1, int lineTo2, int lineFrom2) {
    m_WFF = w;
    m_LineNum = lineNum;
    m_DJLine = disLine;
    m_LineTo1 = lineTo1;
    m_LineTo2 = lineTo2;
    m_LineFrom1 = lineFrom1;
    m_LineFrom2 = lineFrom2;    
  }
  
  public WFF getWFF() {
    return m_WFF;
  }

  public String getRule() {
    return "DE";
  }

  public String getReason() {
    return m_DJLine + "," + m_LineTo1 + "-" + m_LineFrom1 + ", " + m_LineTo2 + "-" + m_LineFrom2 + ", " + "DE";
  }

  public int getLineNum() {
    return m_LineNum;
  }
  public boolean isHyp() {
    return false;
  }

}