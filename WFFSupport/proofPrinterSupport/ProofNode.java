package WFFSupport.proofPrinterSupport;

import WFFSupport.*;

public interface ProofNode {
  public WFF getWFF();
  public String getRule();
  public String getReason();
  public int getLineNum();
  public boolean isHyp();
}