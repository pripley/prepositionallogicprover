package WFFSupport.proofPrinterSupport;

import WFFSupport.*;

public class ReitNode implements ProofNode {
  private WFF m_WFF;
  private int m_Line;
  private int m_LineNum;
  
  public ReitNode(int lineNum, WFF w, int line) {
    m_WFF = w;
    m_Line = line;
    m_LineNum = lineNum;
  }
  
  public WFF getWFF() {
    return m_WFF;
  }
  public String getRule() {
    return "reit";
  }
  public String getReason() {
    return m_Line + "," + getRule();
  }
  public int getLineNum() {
    return m_LineNum;
  }
  public boolean isHyp() {
    return false;
  }

}