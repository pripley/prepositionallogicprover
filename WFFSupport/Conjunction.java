package WFFSupport;

public class Conjunction implements LogicalParticle {
  public boolean want() {
    return true;
  }
  public void printParticle() {
    System.out.println("^"); 
  }

  public String getParticle() {
    return("^"); 
  }

  public boolean equals(LogicalParticle l) {
    return l.getParticle().equals("^");
  }
}