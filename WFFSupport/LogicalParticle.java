package WFFSupport;

public interface LogicalParticle {
  public boolean want();
  public void printParticle();
  public String getParticle();
  public boolean equals(LogicalParticle l);
}