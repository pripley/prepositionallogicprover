package WFFSupport;

import java.util.*;

/**
* Factory class for WFF
* Takes in a string and produces a WFF
*/
public class WFFFactory {
  private final String m_LPs = "#>~V^()";

  public WFFFactory() {}
  
  public WFF makeWFF(String s) throws BadWFFException {
    if (s.length() == -1) { throw new BadWFFException(); }
    StringTokenizer t = new StringTokenizer(s, m_LPs, true);   
    return buildWFF(t, null);
  }
  
  private WFF buildWFF(StringTokenizer s, WFF root) {
    String currentChar;
    while (s.hasMoreTokens()) {
      currentChar = s.nextToken();
      //if letter == LP set the root node equal to the leftWFF of the LP WFF
      if (m_LPs.indexOf(currentChar) > -1) {
        //if char is "(" recurse
        if (currentChar.equals("(")) {
          
          if (root != null) {
            WFF w1 = buildWFF(s, null);
          }
          
        }   
        //if char is ")" recurse
        if (currentChar.equals(")")) {
//System.out.println("returning root via ):"+root.getFormula());
          return root;
        }
        if ((!currentChar.equals(")")) && (!currentChar.equals("("))) {
          LogicalParticle lp = LPFactory.makeLP(currentChar);
      
          WFF rw = buildWFF(s, null); //get the right WFF of the logical particle

//System.out.println("making new root with:"+root.getFormula()+ " as the left formula");
//System.out.println("making new root with:"+rw.getFormula()+ " as the right formula");
          WFF w = new WFF(lp, root, rw);
          w.setLeftWFF(root);
          w.setRightWFF(rw);
          //if (root == null) { root = w; }
          root = w;
        }
      }  
      else {
      
        //if letter make WFF if lst char make root
        WFF a = new WFF(currentChar);
        root = a;
      }
    }
    return root;
  }
}

