package WFFSupport;

public class Disjunction implements LogicalParticle {
  public boolean want() {
    return true;
  }
  public void printParticle() {
    System.out.println("V"); 
  }

  public String getParticle() {
    return("V"); 
  }

  public boolean equals(LogicalParticle l) {
    return l.getParticle().equals("V");
  }
}