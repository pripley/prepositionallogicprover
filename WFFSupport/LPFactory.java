package WFFSupport;

public class LPFactory {

  public static LogicalParticle makeLP(String s) {
    LogicalParticle p;
    if(s.equals(">")) { 
      p = new HorseShoe();
    }
    else if (s.equals("#")) {
      p = new Biconditional();
    }
    else if (s.equals("^")) {
      p = new Conjunction();
    }
    else if (s.equals("V")) {
      p = new Disjunction();
    }
    else {
      p = new Negation();
    }
    return p;
  }

}