import java.io.*;
import WFFSupport.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.event.*;
import java.util.*;

public class GUILogic extends JFrame implements ItemListener {
  private final JPanel m_Panel;
  private final ArrayList m_Boxes;
  private final ArrayList m_TFields;

  public GUILogic(String s) {
    this.setTitle(s);
    m_Boxes = new ArrayList();
    m_TFields = new ArrayList();

    this.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }  
    });
    
    m_Panel = new JPanel(new GridLayout(0,2));
    JPanel z = new JPanel();  //spacer for want label
    JLabel zz = new JLabel();  //spacer for want label
    z.add(zz);
    JLabel w = new JLabel("Want:");
    z.add(w);
    final JTextField t0 = new JTextField(10);
    m_Panel.add(z);
    m_Panel.add(t0);

    for (int i=0; i<5; i++) {
      final JTextField t = new JTextField(10);
      t.disable();
      JPanel j = new JPanel();
      JCheckBox c = new JCheckBox();
      c.addItemListener(this);
      JLabel h = new JLabel("Given:");
      j.add(c);
      j.add(h);
      m_Panel.add(j);
      m_Panel.add(t);
      m_Boxes.add(c);
      m_TFields.add(t);
    }   
    JButton b = new JButton("Generate Proof");
    b.addMouseListener(new MouseAdapter() {
      WFFFactory m_Fact = new WFFFactory();  
      ProofGenerator m_Gen = new ProofGenerator();

      public void mouseClicked(MouseEvent e) {
        System.out.println("And so it begins...");
        try {
          if (t0.getText().trim().equals("")) {
            System.out.println("You need to have a want");
            return;
          }
       
          ArrayList downScope = new ArrayList();
          WFF w = m_Fact.makeWFF(t0.getText());
          System.out.println("WFF:"+w.getFormula());
          WFF w1;
          JTextField t;
          JCheckBox c;
          ListIterator k = m_Boxes.listIterator();
          while (k.hasNext()) { //only want unique NI assumptions
            c = (JCheckBox)k.next();   
            if (c.isSelected()) {
               
               t = (JTextField)m_TFields.get(k.nextIndex()-1);
               w1 = m_Fact.makeWFF(t.getText());
               downScope.add(w1);
            }
          }
          if (m_Gen.newProof(w, downScope)) { System.out.println("success"); } else { System.out.println("failure"); }

        } catch(BadWFFException er) {
          System.out.println("The formula was not well-formed.");
        
        } catch(Exception er1) {
          System.out.println("The formula was not well-formed. "+er1.getMessage());
          er1.printStackTrace();
        }
      }
    });

    m_Panel.add(b);
    getContentPane().add(m_Panel);
    pack();
    setVisible(true);
  }

  public void itemStateChanged(ItemEvent e) {
    //System.out.println("state changed");
    Object s = e.getItemSelectable();
    JCheckBox c = (JCheckBox)s;
    JTextField t = (JTextField)m_TFields.get(m_Boxes.indexOf(s));
    if (c.isSelected()) {
       t.enable();
    }
    else {
       t.disable();
    }
  }
  
  public static void main(String arg[]) {
    GUILogic g = new GUILogic("Logic");
  }
}