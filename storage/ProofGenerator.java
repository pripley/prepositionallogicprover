package WFFSupport;

import java.util.*;
import WFFSupport.proofPrinterSupport.*;

public class ProofGenerator {
  private ArrayList m_Proof;
  private SVGProofPrinter m_SVGPrinter;
  private int m_LineNum;
  private int m_HaveItNum;
  private boolean m_HaveIt; 
  private ArrayList m_PrevWants;
  private int m_LastUniqueLine;

  /**
  *  calls the recursive function which does the proof.  If the proof is successful the proof is printed out.
  */
  public boolean newProof(WFF p, ArrayList onScope) {
    m_Proof = new ArrayList();
    m_HaveItNum = 1;
    m_LineNum = 1;
  
    m_HaveIt = false;
    m_SVGPrinter = new SVGProofPrinter();
    ArrayList downScope = new ArrayList();
    ArrayList bicondLock = new ArrayList();
    m_PrevWants = new ArrayList();
    Iterator i = onScope.iterator();
    WFF q;
    while (i.hasNext()) {
      q = (WFF)i.next();

      m_Proof.add(new GivenNode(m_LineNum, q));
      m_LineNum++;
    }
    m_LastUniqueLine = m_LineNum - 1;
    boolean b = want (p,m_Proof,downScope, new ArrayList(), onScope, bicondLock);
    if (b) {
      System.out.println("Generating SVG...");
//System.out.println("size:"+m_Proof.size());
      m_SVGPrinter.generateSVG(m_Proof);
    } 
    return b;
  }

  private int getHaveItNumber() {
    if (!m_HaveIt) {
      return -1;
    }
    m_HaveIt = false;
    return m_HaveItNum;
  }


  /**
  *  Main proof generating function: does the want list as well as builds up the proof
  */
  private boolean want(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    int saveLastUnique = m_LastUniqueLine;
    System.out.println("Want:"+p.getFormula());
    WFF q;
    //check if you have p on the current scope
    
    if (haveIt(p, hyp,onScope)) { return true; }
    
    //check if you could reiterate p from the downScope
    if (reit(p,hyp,downScope, onScope)) { return true; }

    //circle check
    WFF w;
    DSMarker d;
    Iterator k = m_PrevWants.iterator();
    while (k.hasNext()) { 
      d = (DSMarker)k.next();
      w = d.getWFF();
System.out.println("Want List: "+ w.getFormula() + " m_LastUnique:"+m_LastUniqueLine+" wantLine:"+d.getLineNum());    

      if ((w.equals(p)) && (d.getLineNum() >= m_LastUniqueLine)) { 
        System.out.println("rejected");
        
        return false;
      }
    }

    m_PrevWants.add(new DSMarker(p, m_LineNum-1));
 

    //Smaller Part of Something?
    ArrayList a = new ArrayList(onScope);
    Iterator j = a.iterator();
    while (j.hasNext()) {

      q = (WFF)j.next();
      if ((!q.isLetter()) && (q.contains(p))) {
        //Negation Elimination

        if (q.getMainLogicalParticle().getParticle().equals("~")) {
          if (NE(p,q,hyp,downScope,NIAssumed,onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }          
        }

        //HorseShoe
        if (q.getMainLogicalParticle().getParticle().equals(">")) {
          if (MP(p,q,hyp,downScope,NIAssumed,onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }          
        }

        //Biconditional Elimination
        if (q.getMainLogicalParticle().getParticle().equals("#")) {
          if (BE(p,q,hyp,downScope,NIAssumed,onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }          
        }
        //Conjunction Elimination
        if (q.getMainLogicalParticle().getParticle().equals("^")) {
          if (CE(p,q,hyp,downScope,NIAssumed,onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }          
        }
      }
m_LastUniqueLine = saveLastUnique;
    }
    a = new ArrayList(downScope);
    j = a.iterator();
    ArrayList g = new ArrayList(onScope);
    while (j.hasNext()) {

      q = (WFF)((DSMarker)j.next()).getWFF();

      //make sure not already done by onscope
      boolean b = false;
      
      Iterator f = g.iterator();
      WFF wf;
      while (f.hasNext()) {

        wf = (WFF)f.next();
        if (wf.equals(q)) {
          b = true;
          break;
        }
      }
      if (!b) {
        
g.add(q);      
        if ((!q.isLetter()) && (q.contains(p))) {
          //Negation Elimination
  
          if (q.getMainLogicalParticle().getParticle().equals("~")) {
            if (NE(p,q,hyp,downScope,NIAssumed,onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }          
          }

          //HorseShoe
          if (q.getMainLogicalParticle().getParticle().equals(">")) {
            if (MP(p,q,hyp,downScope,NIAssumed,onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }          
          }

          //Biconditional Elimination
          if (q.getMainLogicalParticle().getParticle().equals("#")) {
            if (BE(p,q,hyp,downScope,NIAssumed,onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }          
          }
          //Conjunction Elimination
          if (q.getMainLogicalParticle().getParticle().equals("^")) {
            if (CE(p,q,hyp,downScope,NIAssumed,onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }          
          }
        }
      }
m_LastUniqueLine = saveLastUnique;
    }


    System.out.println("Intro Rules?");    
    //Intro Rules?
    if (!p.isLetter()) {
      if (p.getMainLogicalParticle().getParticle().equals(">")) {

        if (II(p,hyp,downScope,NIAssumed,onScope, bicondLock)) {  
           m_PrevWants.remove(m_PrevWants.size()-1); 
           return true; 
        } else { 
          System.out.println("failure end hyp:"+p.getLeftWFF().getFormula());
        }
      }

      if (p.getMainLogicalParticle().getParticle().equals("~")) {
      
        if (NI(p,hyp,downScope,NIAssumed, onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }
      }

      if (p.getMainLogicalParticle().getParticle().equals("#")) {      
        if (BI(p,hyp,downScope,NIAssumed, onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }
      }

      if (p.getMainLogicalParticle().getParticle().equals("^")) {      
        if (CI(p,hyp,downScope,NIAssumed, onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }
      }
      if (p.getMainLogicalParticle().getParticle().equals("V")) {      
        if (DI(p,hyp,downScope,NIAssumed, onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }
      }

    }
m_LastUniqueLine = saveLastUnique; 
    //Reductio
    
     
    if (reductio(p,hyp,downScope,NIAssumed,onScope, bicondLock)) { m_PrevWants.remove(m_PrevWants.size()-1); return true; }

    m_PrevWants.remove(m_PrevWants.size()-1);
    m_LastUniqueLine = saveLastUnique;
    return false;
  }


  /**
  *  checks if the current Scope has p
  */
  private void isUnique(WFF p, ArrayList onScope, ArrayList downScope) {
    WFF q;
    Iterator j = onScope.iterator();
    while (j.hasNext()) {

      q = (WFF)j.next();
      if (q.equals(p)) {
        return;
      }
    }
    j = downScope.iterator();
    while (j.hasNext()) {

      q = (WFF)((DSMarker)j.next()).getWFF();
      if (q.equals(p)) {
        return;
      }
    }
    m_LastUniqueLine = m_LineNum - 1;
  }
 
  /**
  *  checks if the current Scope has p
  */
  private boolean haveIt(WFF p, ArrayList hyp, ArrayList onScope) {
    ProofNode n;
    WFF q;
    ListIterator i = hyp.listIterator();

    while (i.hasNext()) {
      n = (ProofNode)i.next();
      q = n.getWFF();
      if ((!n.isHyp()) && (p.equals(q))) {
        //System.out.println(q.getFormula()+":"+p.getFormula()+":"+n.getLineNum()+" is in current scope");
        m_HaveItNum = n.getLineNum();
        m_HaveIt = true;
        return true;
      }
      
    }
    return false;
  }


  /**
  *  Tries to use Reiteration to get p
  */
  private boolean reit(WFF p, ArrayList hyp, ArrayList downScope, ArrayList onScope) {
    WFF q;
    DSMarker m;
    ListIterator i = downScope.listIterator();
    
    while (i.hasNext()) {
      m = (DSMarker)i.next();
      q = m.getWFF();
      
      if (p.equals(q)) {
        //System.out.println(q.getFormula()+":"+p.getFormula()+":"+count+":"+(i.nextIndex()-1)+" can be reit from a down scope");
        
        hyp.add(new ReitNode(m_LineNum,q,m.getLineNum()));
        onScope.add(q);
        m_LineNum++;
        //m_HaveIt = false;
        return true;
      }
      
    }
    return false;

  }

  /**
  *  Tries to do Reductio on p
  */
  private boolean reductio(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    WFF w1;
    WFF w2;
    if ((!p.isLetter()) && (p.getMainLogicalParticle().getParticle().equals("~"))) {
      System.out.println("you failed in NI so you fail here"); 
      return false;
    }
    System.out.println("When all else fails try Reductio:"+p.getFormula()); 
    
    LogicalParticle l1 = new Negation(); //add 2 ~'s to p, the WFF you want, and use NI to try and get it.
    LogicalParticle l2 = new Negation();
    w1 = new WFF(l1, null, p); 
    w2 = new WFF(l2, null, w1); 
    
    if (NI(w2,hyp,downScope, NIAssumed, onScope, bicondLock)) {
      if (NE(p,w2, hyp, downScope,NIAssumed, onScope, bicondLock)) {
        System.out.println("Reductio success");
        return true;
      }
      return false;
    }
    else {
      return false;
    } 
  }



  /**
  *  Tries to use BE on q in order to get p
  */
  private boolean BE(WFF p, WFF q, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    boolean bool = false;  
    int saveLastUnique = m_LastUniqueLine;
    int saveLineNum = m_LineNum - 1;
    int saveNextLineNum = m_LineNum;
    ArrayList savehypList = new ArrayList(hyp);
    WFF w;
    Iterator k = bicondLock.iterator();
    while (k.hasNext()) { 
      w = (WFF)k.next();    
      if (w.equals(q)) { return false; }
    }
     
    if (q.getRightWFF().contains(p)) {
      
 //     if (q.getLeftWFF().equals(p)) { break; }  //the other while loop would be better for handling it
      bicondLock.add(q);
      bool =  want(q.getLeftWFF(), hyp, downScope, NIAssumed, onScope, bicondLock);
      bicondLock.remove(q);
      
      if (m_HaveIt) {
        saveLineNum = getHaveItNumber();
      }
      else {
        saveLineNum = m_LineNum - 1;
      }
        
      //m_HaveIt = false;      
      if (!bool) { return false; }
      else { 
        System.out.println("use B.E. on " + q.getFormula() + " and "+ q.getLeftWFF().getFormula());

        isUnique(q.getRightWFF(), onScope, downScope);
        onScope.add(q.getRightWFF());
        want(q, hyp, downScope, NIAssumed, onScope, bicondLock);        

        int n = 0;
        if (m_HaveIt) {
          n = getHaveItNumber();
        }
        else {
          n = m_LineNum - 1;

        }
        //m_HaveIt = false;

        hyp.add(new BENode(m_LineNum,q.getRightWFF(),saveLineNum,n));  
        m_LineNum++;        
        if (p.equals(q.getRightWFF())) { return true; }
//        if ((!q.getRightWFF().isLetter()) && (!q.getRightWFF().getMainLogicalParticle().getParticle().equals("#"))) {
//          q = q.getRightWFF();
//        } else { 
//          return false;
//        }
else {
  bool = want(p, hyp, downScope, NIAssumed, onScope, bicondLock);        
  if (!bool) { 
    m_LineNum = saveNextLineNum;
    hyp.clear();
    hyp.addAll(savehypList);
    m_LastUniqueLine = saveLastUnique;
  }
  return bool;
}
      }
    }

    if (q.getLeftWFF().contains(p)) {
      

      bicondLock.add(q);
      bool =  want(q.getRightWFF(), hyp, downScope, NIAssumed, onScope, bicondLock);
      bicondLock.remove(q);
      saveLineNum = m_LineNum - 1;



      if (m_HaveIt) {
        saveLineNum = getHaveItNumber();
      }
      else {
        saveLineNum = m_LineNum - 1;
      }

        
      //m_HaveIt = false;      
      if (!bool) { 
        return false;
      }
      else { 
        System.out.println("use B.E. on " + q.getFormula() + " and "+ q.getRightWFF().getFormula());

        isUnique(q.getLeftWFF(), onScope, downScope);
        onScope.add(q.getLeftWFF());

        want(q, hyp, downScope, NIAssumed, onScope, bicondLock);        

        int n = 0;
        if (m_HaveIt) {
          n = getHaveItNumber();
        }
        else {
          n = m_LineNum - 1;

        }
        //m_HaveIt = false;

        hyp.add(new BENode(m_LineNum,q.getLeftWFF(),saveLineNum,n));  
        m_LineNum++;        
        if (p.equals(q.getLeftWFF())) { return true; } //check for a negation in front of a letter
//        if ((!q.getLeftWFF().isLetter()) && (!q.getLeftWFF().getMainLogicalParticle().getParticle().equals("#"))) {
//          q = q.getLeftWFF();
//        } else { 
//          return false;
//        }
else {
  bool =  want(p, hyp, downScope, NIAssumed, onScope, bicondLock);        
  if (!bool) {
    m_LineNum = saveNextLineNum;
    hyp.clear();
    hyp.addAll(savehypList);
    m_LastUniqueLine = saveLastUnique;
 }
  return bool;
}

      }
    }


    return false;  
  }

  /**
  *  Tries to use Modeus Poneins on q in order to get p
  */
  private boolean MP(WFF p, WFF q, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    int saveLastUnique = m_LastUniqueLine;
    boolean bool = false;
    int saveNextLineNum = m_LineNum;
    int saveLineNum = m_LineNum - 1;
    ArrayList savehypList = new ArrayList(hyp);
    if (q.getRightWFF().contains(p)) {
      
      bool =  want(q.getLeftWFF(), hyp, downScope, NIAssumed, onScope, bicondLock);

      if (m_HaveIt) {
        saveLineNum = getHaveItNumber();
      }
      else {
        saveLineNum = m_LineNum - 1;
      }

        
      //m_HaveIt = false;      
      if (!bool) { return false; }
      else { 
        System.out.println("use M.P. on " + q.getFormula() + " and "+ q.getLeftWFF().getFormula());
        
        isUnique(q.getRightWFF(), onScope, downScope);
        onScope.add(q.getRightWFF());
   
        want(q, hyp, downScope, NIAssumed, onScope, bicondLock);        
        int n = 0;
        if (m_HaveIt) {
          n = getHaveItNumber();
        }
        else {
          n = m_LineNum - 1;
        }
        //m_HaveIt = false;
 
        hyp.add(new MPNode(m_LineNum,q.getRightWFF(),saveLineNum,n));  
        m_LineNum++;        
        if (p.equals(q.getRightWFF())) {   return true; } //check for a negation in front of a letter
//        if ((!q.getRightWFF().isLetter()) && (q.getRightWFF().getMainLogicalParticle().getParticle().equals(">"))) {
//          q = q.getRightWFF();
//        } else { 
//          return false; //changed
//        }
else {
  bool = want(p, hyp, downScope, NIAssumed, onScope, bicondLock);
  if (bool) {
    
    return true;
  }
  else {
    m_LineNum = saveNextLineNum;
    hyp.clear();
    hyp.addAll(savehypList);
    m_LastUniqueLine = saveLastUnique;
    return false;
  }        
}

      }
    }
    return false;
  }

  /**
  *  Tries to use an Biconditional Introduction to get p
  */  
  private boolean BI(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    boolean bool = false;
    int saveNextLineNum = m_LineNum;
    int saveLineNum = m_LineNum;
 int saveLastUnique = m_LastUniqueLine;
    ArrayList saveOnScope = new ArrayList(onScope);
    ArrayList saveHyp = new ArrayList(hyp);
    LogicalParticle lp = new HorseShoe();
    WFF w = new WFF(lp, p.getRightWFF(), p.getLeftWFF());
    bool = want(w,hyp,downScope, NIAssumed, onScope, bicondLock);
    
    if (m_HaveIt) {
      saveLineNum = getHaveItNumber();
    }
    else {
      saveLineNum = m_LineNum - 1;
    }
    
    //m_HaveIt = false;
    if (bool) {
      
      lp = new HorseShoe();
      WFF w2 = new WFF(lp, p.getLeftWFF(), p.getRightWFF());   
      bool = want(w2,hyp,downScope, NIAssumed, onScope, bicondLock);

      int n = 0;
      if (m_HaveIt) {
        n = getHaveItNumber();
      }
      else {
        n = m_LineNum - 1;
      }

      //m_HaveIt = false;
      if (bool) {
        
        hyp.add(new BINode(m_LineNum, p, n, saveLineNum));
        isUnique(p, onScope, downScope);
        onScope.add(p);

        m_LineNum++;
        return true;
      }
      else {
        m_LastUniqueLine = saveLastUnique;
        m_LineNum = saveNextLineNum;
        onScope.clear();
        hyp.clear();
        onScope.addAll(saveOnScope);
        hyp.addAll(saveHyp); 
        return false;
      }
    }
    else {
      m_LastUniqueLine = saveLastUnique;
      m_LineNum = saveNextLineNum;
      return false;
    }
  }

  /**
  *  Tries to use an Implication Introduction to get p
  */  
  private boolean II(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    ArrayList newhyp = new ArrayList();
    ArrayList newDownScope = new ArrayList(downScope);
    int saveLineNum = m_LineNum;
    int saveLastUnique = m_LastUniqueLine;  

    Iterator j = onScope.iterator();
    WFF w;
    int i = 0;
    while (j.hasNext()) { 
      w = (WFF)j.next();
      newDownScope.add(new DSMarker(w, m_LineNum - (onScope.size() - i)));
      i++;
    }
    ArrayList newOnScope = new ArrayList();
    isUnique(p.getLeftWFF(), newOnScope, newDownScope); //see if the new assumption is unique
    newOnScope.add(p.getLeftWFF());

    System.out.println("try: assume:"+p.getLeftWFF().getFormula());
    

    newhyp.add(new ReitNode(m_LineNum, p.getLeftWFF(), 0));
    m_LineNum++;
    boolean b = want(p.getRightWFF(), newhyp, newDownScope, NIAssumed, newOnScope, new ArrayList());
    newhyp.remove(0);
    //m_HaveIt = false;
  
    if (b) {
      
      hyp.add(new HypNode(saveLineNum, p.getLeftWFF(),newhyp));
      
      hyp.add(new IINode(m_LineNum,p, saveLineNum, m_LineNum - 1));
      m_LineNum++;
    }
    else { 
      m_LineNum = saveLineNum;
      m_LastUniqueLine = saveLastUnique;
    }
      
    return b;
  }

  /**
  *  Tries to use an Negation Elimination to get p
  */  
  private boolean NE(WFF p, WFF q, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    if (q.getRightWFF().isLetter()) { return false; }
    WFF f;
    while (q.getRightWFF().getMainLogicalParticle().getParticle().equals("~")) {
      System.out.println("use NE on "+q.getFormula());
      f = q;
      q = q.getRightWFF().getRightWFF();
      isUnique(q, onScope, downScope);
      onScope.add(q);  

      want(f, hyp, downScope, NIAssumed, onScope, bicondLock);
      int n;
      if (m_HaveIt) {
        n = getHaveItNumber();
      }
      else {
        n = m_LineNum - 1;
      }
      //m_HaveIt = false;
      if (p.equals(q)) { 
        hyp.add(new NENode(m_LineNum,q,n));
        m_LineNum++;        
        return true;
      }
      if ((q.isLetter()) || (q.getRightWFF().isLetter()) || (!q.getMainLogicalParticle().getParticle().equals("~"))) {
        return false;
      }
      
    }
    return false;
  }

  /**
  *  Tries to use Negation Introduction to get p
  */  
  private boolean NI(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    ArrayList newhyp = new ArrayList();
    ArrayList newDownScope = new ArrayList(downScope);

    Iterator h = onScope.iterator();
    WFF w;
    int x = 0;
    while (h.hasNext()) { 
      w = (WFF)h.next();
      newDownScope.add(new DSMarker(w, m_LineNum - (onScope.size() - x)));
      x++;
    }


    ArrayList newOnScope = new ArrayList();
    System.out.println("try NI:assume opposite of:"+p.getFormula());
    boolean b = false;
    int saveLineNum = m_LineNum;
    int saveContraNum = 0;
    int saveContraNum2 = 0;
  
    int saveLastUnique = m_LastUniqueLine;

    newhyp.add(new ReitNode(saveLineNum, p.getRightWFF(), 0));

    m_LineNum++;    
    isUnique(p.getRightWFF(), newOnScope, newDownScope);
    newOnScope.add(p.getRightWFF());


    WFF q;
    LogicalParticle l;

    //Check to see if NI has already been tried on this formula
    Iterator k = NIAssumed.iterator();
    while (k.hasNext()) { //only want unique NI assumptions
      w = (WFF)k.next();    
      if (w.equals(p.getRightWFF())) { m_LineNum = saveLineNum; m_LastUniqueLine = saveLastUnique; return false; }
    }
    
    //add the new NI assumption to the NIAssumed list
    NIAssumed.add(p.getRightWFF());

    ArrayList a = new ArrayList();
    
    Iterator j1 = onScope.iterator();
    while (j1.hasNext()) { //only want unique WFF when looking for contradictions
      w = (WFF)j1.next();
      a = addToSet(w, a);
    }

    Iterator j = downScope.iterator();
    while (j.hasNext()) { //only want unique WFF when looking for contradictions
      w = (WFF)((DSMarker)j.next()).getWFF();
      a = addToSet(w, a);
    }


    Iterator i = a.iterator();
int contraSaveLastUnique = m_LastUniqueLine;
    while (i.hasNext()) {
      m_LastUniqueLine = contraSaveLastUnique;
      q = (WFF)i.next();
      
      l = new Negation();
      w = new WFF(l, null, q);

      if (want(q, newhyp, newDownScope, NIAssumed, newOnScope, new ArrayList())) { 

        if (m_HaveIt) {
          saveContraNum = getHaveItNumber();
        } else {
          saveContraNum = m_LineNum - 1;
        }
        NIAssumed.add(q); 
        if (want(w, newhyp, newDownScope, NIAssumed, newOnScope, new ArrayList())) {
          NIAssumed.remove(NIAssumed.size()-1);
          if (m_HaveIt) {

            saveContraNum2 = getHaveItNumber();
          }
          else {
            saveContraNum2 = m_LineNum - 1;
          }

          System.out.println("got a contradiction with "+q.getFormula()+" and "+w.getFormula());
          b = true;
          newhyp.remove(0);        
          break;
        }
        else {
          NIAssumed.remove(NIAssumed.size()-1);
m_LastUniqueLine = contraSaveLastUnique;
          newhyp.clear();
          newhyp.add(new ReitNode(saveLineNum, p.getRightWFF(), 0)); //Bogus proof node for the hyp to be found when checking if on current scope
          newOnScope.clear();
          newOnScope.add(p.getRightWFF());
          m_LineNum = saveLineNum + 1;
        }
      } 
    }


    NIAssumed.remove(NIAssumed.size()-1);
    if (b) {
      isUnique(p, onScope, downScope);
      onScope.add(p);

      
      hyp.add(new HypNode(saveLineNum,p.getRightWFF(), newhyp));      
      
      hyp.add(new NINode(m_LineNum,p, saveLineNum,saveContraNum,saveContraNum2));
      m_LineNum++;
      //m_HaveIt = false;
    } else { 
      newhyp.clear();
      m_LineNum = saveLineNum;
m_LastUniqueLine = saveLastUnique;
      System.out.println("Unable to find a contradiction when "+p.getRightWFF().getFormula()+" was assumed");
    }
    return b;
  }

  /**
  * used by NI to get unique WFFs for finding contradictions
  */
  private ArrayList addToSet(WFF w, ArrayList a) {
    WFF w2;
    if (!w.isLetter()) {
      if (w.getRightWFF() != null) { a = addToSet(w.getRightWFF(),a); }
      if (w.getLeftWFF() != null) { a = addToSet(w.getLeftWFF(),a); }  
    } 
    if ((w.isLetter()) || (!(w.getMainLogicalParticle().getParticle().equals("~")))) {      
    //if (w.isLetter()) {      
      Iterator j = a.iterator();
      boolean b = true;
      while (j.hasNext()) { //only want unique WFF when looking for contradictions
        w2 = (WFF)j.next();
        if (w2.equals(w)) {
          b = false;
          break;
        }
      }
      if (b) {
        a.add(w);
      }     
    }
    return a;    
  }  


  /**
  *  Tries to use CE on q in order to get p
  */
  private boolean CE(WFF p, WFF q, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {  
    if ((q.getRightWFF().equals(p)) || (q.getLeftWFF().equals(p))) {
      onScope.add(p);
      want(q, hyp, downScope, NIAssumed, onScope, bicondLock);        

      int n = 0;
      if (m_HaveIt) {
        n = getHaveItNumber();
      }
 
      hyp.add(new CENode(m_LineNum, p, n));
      m_LineNum++;      
      return true;
    }
    else {
      return false;
    }
  }

  /**
  *  Tries to use DE on q in order to get p
  */
  private boolean DE(WFF p, WFF q, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {  
    //assume left side of q want p
    
    
    //assume right side of q want p  
    
    ArrayList newhyp = new ArrayList();
    ArrayList newDownScope = new ArrayList(downScope);
    //newDownScope.addAll(onScope);
    Iterator j = onScope.iterator();
    WFF w;
    int i = 0;
    while (j.hasNext()) { 
      w = (WFF)j.next();
      newDownScope.add(new DSMarker(w, m_LineNum - (onScope.size() - i)));
      i++;
    }
    ArrayList newOnScope = new ArrayList();
    newOnScope.add(p.getLeftWFF());
    System.out.println("try: assume:"+p.getLeftWFF().getFormula());
    int saveLineNum = m_LineNum;
  
    
    m_LineNum++;
    newhyp.add(new ReitNode(saveLineNum, p.getLeftWFF(), 0));
    boolean b = want(p.getRightWFF(), newhyp, newDownScope, NIAssumed, newOnScope, new ArrayList());
    newhyp.remove(0);
    //m_HaveIt = false;
  
    if (b) {
      
      hyp.add(new HypNode(saveLineNum, p.getLeftWFF(),newhyp));
      
      hyp.add(new DENode(m_LineNum,p, saveLineNum, m_LineNum - 1,0,0,0));
      m_LineNum++;
    }
    else { m_LineNum = saveLineNum + 1; }
      
    return b;

  }

  /**
  *  Tries to use CI in order to get p
  */
  private boolean CI(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {  
    //want left and right sides of p
    boolean bool = false;
    int saveLineNum = 0;
    ArrayList saveOnScope = new ArrayList(onScope);
    ArrayList saveHyp = new ArrayList(hyp);
    
    bool = want(p.getLeftWFF(),hyp,downScope, NIAssumed, onScope, bicondLock);
    
    if (m_HaveIt) {
      saveLineNum = getHaveItNumber();
    }
    else {
      saveLineNum = m_LineNum - 1;
    }

    //m_HaveIt = false;
    if (bool) {
      
      bool = want(p.getRightWFF(), hyp, downScope, NIAssumed, onScope, bicondLock);
      int n = 0;
      if (m_HaveIt) {
        n = getHaveItNumber();
      }
      else {
        n = m_LineNum - 1;
      }

      //m_HaveIt = false;
      if (bool) {
        //want(p.getRightWFF(), hyp, downScope, NIAssumed, onScope, bicondLock);        


        hyp.add(new CINode(m_LineNum, p, n, saveLineNum));
        onScope.add(p);

        m_LineNum++;
        return true;
      }
      else {
        m_LineNum = saveLineNum;
        onScope.clear();
        hyp.clear();
        onScope.addAll(saveOnScope);
        hyp.addAll(saveHyp); 
        return false;
      }
    }
    else {
      return false;
    }
  }

  /**
  *  Tries to use DI in order to get p
  */
  private boolean DI(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {  
    //want left or right sides of p
    boolean bool = false;
    int saveLineNum = 0;
    ArrayList saveOnScope = new ArrayList(onScope);
    ArrayList saveHyp = new ArrayList(hyp);
    
    bool = want(p.getLeftWFF(),hyp,downScope, NIAssumed, onScope, bicondLock);
    
    if (m_HaveIt) {
      saveLineNum = getHaveItNumber();
    }
    else {
      saveLineNum = m_LineNum - 1;
    }

    //m_HaveIt = false;
    if (bool == false) {
      m_LineNum = saveLineNum;
      onScope.clear();
      hyp.clear();
      onScope.addAll(saveOnScope);
      hyp.addAll(saveHyp); 
          
      bool = want(p.getRightWFF(), hyp, downScope, NIAssumed, onScope, bicondLock);
      int n = 0;
      if (m_HaveIt) {
        saveLineNum = getHaveItNumber();
      }
      else {
        saveLineNum = m_LineNum - 1;
      }

      if (bool) {
        hyp.add(new DINode(m_LineNum, p, saveLineNum));

        onScope.add(p);
        m_LineNum++;
        return true;
      }

      else {
        m_LineNum = saveLineNum;
        onScope.clear();
        hyp.clear();
        onScope.addAll(saveOnScope);
        hyp.addAll(saveHyp); 
        return false;
      }
    }
    else {
      hyp.add(new DINode(m_LineNum, p, saveLineNum));
      onScope.add(p);
      m_LineNum++;
      return true;
    }
  }



}

class DSMarker {
  WFF m_WFF;
  int m_Line;
  protected DSMarker(WFF w, int lineNum) {
    m_WFF = w;
    m_Line = lineNum;
  }
  protected int getLineNum() {
    return m_Line;
  }
  protected WFF getWFF() {
    return m_WFF;
  }
}