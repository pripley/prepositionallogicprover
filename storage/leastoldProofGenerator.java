package WFFSupport;

import java.util.*;
import WFFSupport.proofPrinterSupport.*;

public class ProofGenerator {
  private ArrayList m_Proof;
  private SVGProofPrinter m_SVGPrinter;
  private int m_LineNum;
  private int m_HaveItNum;
  private boolean m_HaveIt; 

  /**
  *  calls the recursive function which does the proof.  If the proof is successful the proof is printed out.
  */
  public boolean newProof(WFF p, ArrayList onScope) {
    m_Proof = new ArrayList();
    m_HaveItNum = 1;
    m_LineNum = 1;
  
    m_HaveIt = false;
    m_SVGPrinter = new SVGProofPrinter();
    ArrayList downScope = new ArrayList();
    ArrayList bicondLock = new ArrayList();
    Iterator i = onScope.iterator();
    WFF q;
    while (i.hasNext()) {
      q = (WFF)i.next();

      m_Proof.add(new GivenNode(m_LineNum, q));
      m_LineNum++;
    }

    boolean b = want (p,m_Proof,downScope, new ArrayList(), onScope, bicondLock);
    if (b) {
      System.out.println("Generating SVG...");
      m_SVGPrinter.generateSVG(m_Proof);
    } 
    return b;
  }

  /**
  *  Main proof generating function: does the want list as well as builds up the proof
  */
  private boolean want(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    System.out.println("Want:"+p.getFormula());
    WFF q;
    //check if you have p on the current scope
    m_HaveIt = false;
    if (haveIt(p, hyp,onScope)) { return true; }
    m_HaveIt = false;
    //check if you could reiterate p from the downScope
    if (reit(p,hyp,downScope, onScope)) { return true; }
 
    //Smaller Part of Something?
    ArrayList a = new ArrayList(onScope);
    Iterator j = a.iterator();
    while (j.hasNext()) {

      q = (WFF)j.next();
      if ((!q.isLetter()) && (q.contains(p))) {
        //Negation Elimination

        if (q.getMainLogicalParticle().getParticle().equals("~")) {
          if (NE(p,q,hyp,downScope,NIAssumed,onScope, bicondLock)) { return true; }          
        }

        //HorseShoe
        if (q.getMainLogicalParticle().getParticle().equals(">")) {
          if (MP(p,q,hyp,downScope,NIAssumed,onScope, bicondLock)) { return true; }          
        }

        //Biconditional Elimination
        if (q.getMainLogicalParticle().getParticle().equals("#")) {
          if (BE(p,q,hyp,downScope,NIAssumed,onScope, bicondLock)) { return true; }          
        }

      }
    }
    System.out.println("Intro Rules?");    
    //Intro Rules?
    if (!p.isLetter()) {
      if (p.getMainLogicalParticle().getParticle().equals(">")) {

        
        if (II(p,hyp,downScope,NIAssumed,onScope, bicondLock)) {            
           return true; 
        } else { 
          System.out.println("failure end hyp:"+p.getLeftWFF().getFormula());
        }
      }

      if (p.getMainLogicalParticle().getParticle().equals("~")) {
      
        if (NI(p,hyp,downScope,NIAssumed, onScope, bicondLock)) { return true; }
      }

      if (p.getMainLogicalParticle().getParticle().equals("#")) {      
        if (BI(p,hyp,downScope,NIAssumed, onScope, bicondLock)) { return true; }
      }


    }

 
    //Reductio
    
     
    if (reductio(p,hyp,downScope,NIAssumed,onScope, bicondLock)) { return true; }
    m_LineNum--;
    return false;
  }

  /**
  *  checks if the current Scope has p
  */
  private boolean haveIt(WFF p, ArrayList hyp, ArrayList onScope) {
    ProofNode n;
    WFF q;
    ListIterator i = hyp.listIterator();

    while (i.hasNext()) {
      n = (ProofNode)i.next();
      q = n.getWFF();
      if (p.equals(q)) {
        //System.out.println(q.getFormula()+":"+p.getFormula()+":"+n.getLineNum()+" is in current scope");
        m_HaveItNum = n.getLineNum();
        m_HaveIt = true;
        return true;
      }
      
    }
    return false;
  }


  /**
  *  Tries to use Reiteration to get p
  */
  private boolean reit(WFF p, ArrayList hyp, ArrayList downScope, ArrayList onScope) {
    WFF q;
    DSMarker m;
    ListIterator i = downScope.listIterator();
    int count = 1;
    while (i.hasNext()) {
      m = (DSMarker)i.next();
      q = m.getWFF();
      //System.out.println(q.getFormula()+":"+count);
      if (p.equals(q)) {
        //System.out.println(q.getFormula()+":"+p.getFormula()+":"+count+":"+(i.nextIndex()-1)+" can be reit from a down scope");
        
        hyp.add(new ReitNode(m_LineNum,q,m.getLineNum()));
        onScope.add(q);
        m_LineNum++;
        m_HaveIt = false;
        return true;
      }
      count++;
    }
    return false;

  }

  /**
  *  Tries to do Reductio on p
  */
  private boolean reductio(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    WFF w1;
    WFF w2;
    if ((!p.isLetter()) && (p.getMainLogicalParticle().getParticle().equals("~"))) {
      System.out.println("you failed in NI so you fail here"); 
      return false;
    }
    System.out.println("When all else fails try Reductio:"+p.getFormula()); 
    
    LogicalParticle l1 = new Negation();
    LogicalParticle l2 = new Negation();
    w1 = new WFF(l1, null, p); 
    w2 = new WFF(l2, null, w1); 
    
    if (NI(w2,hyp,downScope, NIAssumed, onScope, bicondLock)) {
      if (NE(p,w2, hyp, downScope,NIAssumed, onScope, bicondLock)) {
        System.out.println("Reductio success");
        return true;
      }
      return false;
    }
    else {
      return false;
    } 
  }

  /**
  *  Tries to use BE on q in order to get p
  */
  private boolean BE(WFF p, WFF q, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    boolean bool = false;  
    int saveLineNum = 0;
    WFF w;
    Iterator k = bicondLock.iterator();
    while (k.hasNext()) { //only want unique NI assumptions
      w = (WFF)k.next();    
      if (w.equals(q)) { return false; }
    }
    
    while (q.getRightWFF().contains(p)) {
      
      if (q.getLeftWFF().equals(p)) { break; }
      bicondLock.add(q);
      bool =  want(q.getLeftWFF(), hyp, downScope, NIAssumed, onScope, bicondLock);
      bicondLock.remove(q);
      saveLineNum = m_LineNum - 1;
      if (m_HaveIt) {
        saveLineNum = m_HaveItNum;
      }
        
      m_HaveIt = false;      
      if (!bool) { return false; }
      else { 
        System.out.println("use B.E. on " + q.getFormula() + " and "+ q.getLeftWFF().getFormula());
        onScope.add(q.getRightWFF());

        m_LineNum++;    
        want(q, hyp, downScope, NIAssumed, onScope, bicondLock);        

        int n = 0;
        if (m_HaveIt) {
          n = m_HaveItNum;
        }
        //else {
        //  n = m_LineNum - 1;
        //}
        m_HaveIt = false;
        hyp.add(new BENode(m_LineNum - 1,q.getRightWFF(),saveLineNum,n));  
        
        if (p.equals(q.getRightWFF())) { return true; } //check for a negation in front of a letter
        if (!q.getRightWFF().isLetter()) {
          q = q.getRightWFF();
        } else { 
          return true;
        }
      }
    }

    while (q.getLeftWFF().contains(p)) {
      
      if (q.getRightWFF().equals(p)) { break; }
      bicondLock.add(q);
      bool =  want(q.getRightWFF(), hyp, downScope, NIAssumed, onScope, bicondLock);
      bicondLock.remove(q);
      saveLineNum = m_LineNum - 1;



      if (m_HaveIt) {
        saveLineNum = m_HaveItNum;
      }
        
      m_HaveIt = false;      
      if (!bool) { return false; }
      else { 
        System.out.println("use B.E. on " + q.getFormula() + " and "+ q.getRightWFF().getFormula());
        onScope.add(q.getLeftWFF());

        m_LineNum++;    
        want(q, hyp, downScope, NIAssumed, onScope, bicondLock);        

        int n = 0;
        if (m_HaveIt) {
          n = m_HaveItNum;
        }
        //else {
        //  n = m_LineNum - 1;
        //}
        m_HaveIt = false;
        hyp.add(new BENode(m_LineNum - 1,q.getLeftWFF(),saveLineNum,n));  
        
        if (p.equals(q.getLeftWFF())) { return true; } //check for a negation in front of a letter
        if (!q.getLeftWFF().isLetter()) {
          q = q.getLeftWFF();
        } else { 
          return true;
        }
      }
    }


    return false;  
  }

  /**
  *  Tries to use Modeus Poneins on q in order to get p
  */
  private boolean MP(WFF p, WFF q, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    boolean bool = false;  
    int saveLineNum = 0;
    while (q.getRightWFF().contains(p)) {
      
      if (q.getLeftWFF().equals(p)) { break; }
      bool =  want(q.getLeftWFF(), hyp, downScope, NIAssumed, onScope, bicondLock);
      saveLineNum = m_LineNum - 1;
      if (m_HaveIt) {
        saveLineNum = m_HaveItNum;
      }
        
      m_HaveIt = false;      
      if (!bool) { return false; }
      else { 
        System.out.println("use M.P. on " + q.getFormula() + " and "+ q.getLeftWFF().getFormula());
        onScope.add(q.getRightWFF());

        m_LineNum++;    
        want(q, hyp, downScope, NIAssumed, onScope, bicondLock);        
        int n = 0;
        if (m_HaveIt) {
          n = m_HaveItNum;
        }
        else {
          n = m_LineNum - 1;
        }
        m_HaveIt = false;
        hyp.add(new MPNode(m_LineNum - 1,q.getRightWFF(),saveLineNum,n));  
        
        if (p.equals(q.getRightWFF())) { return true; } //check for a negation in front of a letter
        if (!q.getRightWFF().isLetter()) {
          q = q.getRightWFF();
        } else { 
          return true;
        }
      }
    }
    return false;
  }

  /**
  *  Tries to use an Biconditional Introduction to get p
  */  
  private boolean BI(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    boolean bool = false;
    int saveLineNum = 0;
    ArrayList saveOnScope = new ArrayList(onScope);
    ArrayList saveHyp = new ArrayList(hyp);
    LogicalParticle lp = new HorseShoe();
    WFF w = new WFF(lp, p.getRightWFF(), p.getLeftWFF());
    bool = want(w,hyp,downScope, NIAssumed, onScope, bicondLock);
    m_HaveIt = false;
    if (bool) {
      saveLineNum = m_LineNum - 1;
      lp = new HorseShoe();
      w = new WFF(lp, p.getLeftWFF(), p.getRightWFF());   
      bool = want(w,hyp,downScope, NIAssumed, onScope, bicondLock);
      m_HaveIt = false;
      if (bool) {
        hyp.add(new BINode(m_LineNum, p, saveLineNum, m_LineNum - 1));
        onScope.add(p);

        m_LineNum++;
        return true;
      }
      else {
        m_LineNum = saveLineNum;
        onScope = saveOnScope;
        hyp = saveHyp;
        return false;
      }
    }
    else {
      return false;
    }
  }

  /**
  *  Tries to use an Implication Introduction to get p
  */  
  private boolean II(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    ArrayList newhyp = new ArrayList();
    ArrayList newDownScope = new ArrayList(downScope);
    //newDownScope.addAll(onScope);
    Iterator j = onScope.iterator();
    WFF w;
    int i = 0;
    while (j.hasNext()) { 
      w = (WFF)j.next();
      newDownScope.add(new DSMarker(w, m_LineNum - (onScope.size() - i)));
      i++;
    }
    ArrayList newOnScope = new ArrayList();
    newOnScope.add(p.getLeftWFF());
    System.out.println("try: assume:"+p.getLeftWFF().getFormula());
    int saveLineNum = m_LineNum;
  
    
    m_LineNum++;
    newhyp.add(new ReitNode(saveLineNum, p.getLeftWFF(), 0));
    boolean b = want(p.getRightWFF(), newhyp, newDownScope, NIAssumed, newOnScope, new ArrayList());
    newhyp.remove(0);
    m_HaveIt = false;
  
    if (b) {
      
      hyp.add(new HypNode(saveLineNum, p.getLeftWFF(),newhyp));
      
      hyp.add(new IINode(m_LineNum,p, saveLineNum, m_LineNum - 1));
      m_LineNum++;
    }
    else { m_LineNum = saveLineNum + 1; }
      
    return b;
  }

  /**
  *  Tries to use an Negation Elimination to get p
  */  
  private boolean NE(WFF p, WFF q, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    if (q.getRightWFF().isLetter()) { return false; }
    WFF f;
    while (q.getRightWFF().getMainLogicalParticle().getParticle().equals("~")) {
      System.out.println("use NE on "+q.getFormula());
      f = q;
      q = q.getRightWFF().getRightWFF();
      onScope.add(q);  
      want(f, hyp, downScope, NIAssumed, onScope, bicondLock);
      int n;
      if (m_HaveIt) {
        n = m_HaveItNum;
      }
      else {
        n = m_LineNum - 1;
      }
      m_HaveIt = false;
      hyp.add(new NENode(m_LineNum,q,n));
      m_LineNum++;
      if (p.equals(q)) { return true; }
      if ((q.isLetter()) || (q.getRightWFF().isLetter()) || (!q.getMainLogicalParticle().getParticle().equals("~"))) {
        return false;
      }
      
    }
    return false;
  }

  /**
  *  Tries to use Negation Introduction to get p
  */  
  private boolean NI(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed, ArrayList onScope, ArrayList bicondLock) {
    ArrayList newhyp = new ArrayList();
    ArrayList newDownScope = new ArrayList(downScope);
//    newDownScope.addAll(onScope);

    Iterator h = onScope.iterator();
    WFF w;
    int x = 0;
    while (h.hasNext()) { 
      w = (WFF)h.next();
      newDownScope.add(new DSMarker(w, m_LineNum - (onScope.size() - x)));
      x++;
    }


    ArrayList newOnScope = new ArrayList();
    System.out.println("try NI:assume opposite of:"+p.getFormula());
    boolean b = false;
    int saveLineNum = m_LineNum;
    int saveContraNum = 0;
    int saveContraNum2 = 0;
  


    newhyp.add(new ReitNode(saveLineNum, p.getRightWFF(), 0));

    m_LineNum++;    
    newOnScope.add(p.getRightWFF());

    WFF q;
    LogicalParticle l;

    //Check to see if NI has already been tried on this formula
    Iterator k = NIAssumed.iterator();
    while (k.hasNext()) { //only want unique NI assumptions
      w = (WFF)k.next();    
      if (w.equals(p.getRightWFF())) { return false; }
    }
    //m_LineNum++;    
    //add the new NI assumption to the NIAssumed list
    NIAssumed.add(p.getRightWFF());

    ArrayList a = new ArrayList();
    Set s = new HashSet();

    Iterator j1 = onScope.iterator();
    while (j1.hasNext()) { //only want unique WFF when looking for contradictions
      w = (WFF)j1.next();
      a = addToSet(s, w, a);
    }

    Iterator j = downScope.iterator();
    while (j.hasNext()) { //only want unique WFF when looking for contradictions
      w = (WFF)((DSMarker)j.next()).getWFF();
      a = addToSet(s, w, a);
    }


    Iterator i = a.iterator();
    while (i.hasNext()) {

      q = (WFF)i.next();
      
      l = new Negation();
      w = new WFF(l, null, q);
      
      if (want(q, newhyp, newDownScope, NIAssumed, newOnScope, new ArrayList())) { 
        if (m_HaveIt) {
          saveContraNum = m_HaveItNum;
        } else {
          saveContraNum = m_LineNum - 1;
        }
 
        if (want(w, newhyp, newDownScope, NIAssumed, newOnScope, new ArrayList())) {
          if (m_HaveIt) {

            saveContraNum2 = m_HaveItNum;
          }
          else {
            saveContraNum2 = m_LineNum - 1;
          }

          System.out.println("got a contradiction with "+q.getFormula()+" and "+w.getFormula());
          b = true;
          newhyp.remove(0);        
          break;
        }
        else {
          newhyp.clear();
          newhyp.add(new ReitNode(saveLineNum, p.getRightWFF(), 0)); //Bogus proof node for the hyp to be found when checking if on current scope
          newOnScope.clear();
          newOnScope.add(p.getRightWFF());
          m_LineNum = saveLineNum + 1;
        }
      }
    }


    NIAssumed.remove(NIAssumed.size()-1);
    if (b) {
      onScope.add(p);
      
      hyp.add(new HypNode(saveLineNum,p.getRightWFF(), newhyp));      
      
      hyp.add(new NINode(m_LineNum,p, saveLineNum,saveContraNum,saveContraNum2));
      m_LineNum++;
      m_HaveIt = false;
    } else { 
      newhyp.clear();
      m_LineNum = saveLineNum + 1;
      System.out.println("Unable to find a contradiction when "+p.getRightWFF().getFormula()+" was assumed");
    }
    return b;
  }

  /**
  * used by NI to get unique WFFs for finding contradictions
  */
  private ArrayList addToSet(Set s, WFF w, ArrayList a) {
    String str = w.getFormula();
    if ((!(str.substring(0).equals("~"))) && (!s.contains(str))) {
      s.add(str);
      a.add(w);
    } 
    if (!w.isLetter()) {
      
      if (w.getRightWFF() != null) { a = addToSet(s,w.getRightWFF(),a); }
      if ((!w.getMainLogicalParticle().getParticle().equals(">")) && (w.getLeftWFF() != null)) { a = addToSet(s,w.getLeftWFF(),a); }       
    }
    return a;    
  }  
}

class DSMarker {
  WFF m_WFF;
  int m_Line;
  protected DSMarker(WFF w, int lineNum) {
    m_WFF = w;
    m_Line = lineNum;
  }
  protected int getLineNum() {
    return m_Line;
  }
  protected WFF getWFF() {
    return m_WFF;
  }
}