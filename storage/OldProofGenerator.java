package WFFSupport;

import java.util.*;
import WFFSupport.proofPrinterSupport.*;

public class ProofGenerator {
  private ArrayList m_Proof;
  private SVGProofPrinter m_SVGPrinter;
  private int m_LineNum;

  public ProofGenerator() {}

  public boolean newProof(WFF p, ArrayList downScope) {
    m_Proof = new ArrayList();
    m_LineNum = 1;
    m_SVGPrinter = new SVGProofPrinter();
    Iterator i = downScope.iterator();
    WFF q;
    while (i.hasNext()) {
      q = (WFF)i.next();
      m_Proof.add(new GivenNode(m_LineNum, q));
      m_LineNum++;
    }

    boolean b = want (p,m_Proof,downScope, new ArrayList());
    if (b) {
      System.out.println("Generating SVG...");
      m_SVGPrinter.generateSVG(m_Proof);
    } 
    return b;
  }

  private boolean want(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed) {
    System.out.println("Want:"+p.getFormula());
    WFF q;   
    //check if you could reiterate p from the downScope
    if (reit(p,hyp,downScope)) { return true; }
 
    //Smaller Part of Something?
    ArrayList a = new ArrayList(downScope);
    Iterator j = a.iterator();
    while (j.hasNext()) {

      q = (WFF)j.next();
      if ((!q.isLetter()) && (q.contains(p))) {
        //Negation Elimination

        if (q.getMainLogicalParticle().getParticle().equals("~")) {
          if (NE(p,q,hyp,downScope,NIAssumed)) { return true; }          
        }

        //HorseShoe
        if (q.getMainLogicalParticle().getParticle().equals(">")) {
          if (MP(p,q,hyp,downScope,NIAssumed)) { return true; }          
        }

      }
    }
    System.out.println("Intro Rules?");    
    //Intro Rules?
    if (!p.isLetter()) {
      if (p.getMainLogicalParticle().getParticle().equals(">")) {

        
        if (II(p,hyp,downScope,NIAssumed)) { 
           //System.out.println("success end hyp:"+p.getLeftWFF().getFormula());
           
           return true; 
        } else { 
          System.out.println("failure end hyp:"+p.getLeftWFF().getFormula());
        }
      }

      if (p.getMainLogicalParticle().getParticle().equals("~")) {
      
        if (NI(p,hyp,downScope,NIAssumed)) { return true; }
      }


    }

 
    //Reductio
    
     
    if (reductio(p,hyp,downScope,NIAssumed)) { return true; }
    m_LineNum--;
    return false;
  }

  /**
  *  Tries to use Reiteration to get p
  */
  private boolean reit(WFF p, ArrayList hyp, ArrayList downScope) {
    WFF q;
    ListIterator i = downScope.listIterator();
    int count = 1;
    while (i.hasNext()) {
      q = (WFF)i.next();
      System.out.println(q.getFormula()+":"+count);
      if (p.equals(q)) {
        System.out.println(q.getFormula()+":"+p.getFormula()+":"+count+":"+(i.nextIndex()-1)+" is in current scope or can be reit from a down scope");
        hyp.add(new ReitNode(m_LineNum,q,count));
        m_LineNum++;
        return true;
      }
      count++;
    }
    return false;
  }

  /**
  *  Tries to do Reductio on p
  */
  private boolean reductio(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed) {
    WFF w1;
    WFF w2;
    if ((!p.isLetter()) && (p.getMainLogicalParticle().getParticle().equals("~"))) {
      System.out.println("you failed in NI so you fail here"); 
      return false;
    }
    System.out.println("When all else fails try Reductio:"+p.getFormula()); 
    
    LogicalParticle l1 = new Negation();
    LogicalParticle l2 = new Negation();
    w1 = new WFF(l1, null, p); 
    w2 = new WFF(l2, null, w1); 
    
    if (NI(w2,hyp,downScope, NIAssumed)) {
      if (NE(p,w2, hyp, downScope,NIAssumed)) {
        System.out.println("Reductio success");
        return true;
      }
      return false;
    }
    else {
      return false;
    } 
  }

  /**
  *  Tries to use Modeus Poneins on q in order to get p
  */
  private boolean MP(WFF p, WFF q, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed) {
    boolean bool = false;  
    int saveLineNum = 0;
    while (q.getRightWFF().contains(p)) {
      saveLineNum = m_LineNum - 1;
      if (q.getLeftWFF().equals(p)) { break; }
      bool =  want(q.getLeftWFF(), hyp, new ArrayList(downScope), NIAssumed);
      if (!bool) { return false; }
      else { 
        System.out.println("use M.P. on " + q.getFormula() + " and "+ q.getLeftWFF().getFormula());
        downScope.add(q.getRightWFF());
        want(q, hyp, downScope, NIAssumed);
        hyp.add(new MPNode(m_LineNum,q.getRightWFF(),m_LineNum-2,m_LineNum-1));  
        m_LineNum++;
        if (p.equals(q.getRightWFF())) { return true; } //check for a negation in front of a letter
        if (!q.getRightWFF().isLetter()) {
          q = q.getRightWFF();
        } else { 
          return true;
        }
      }
    }
    return false;
  }

  /**
  *  Tries to use an Implication Introduction to get p
  */  
  private boolean II(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed) {
    ArrayList newhyp = new ArrayList();
    //newhyp.add(new HypNode(p.getLeftWFF()));
    downScope.add(p.getLeftWFF());
    System.out.println("try: assume:"+p.getLeftWFF().getFormula());
    int saveLineNum = m_LineNum;
    m_LineNum++;
    boolean b = want(p.getRightWFF(), newhyp, new ArrayList(downScope), NIAssumed);
    if (b) {
      hyp.add(new HypNode(saveLineNum, p.getLeftWFF(),newhyp));
      
      hyp.add(new IINode(m_LineNum,p, saveLineNum, m_LineNum - 1));
      m_LineNum++;
    }
    else { m_LineNum--; }
    downScope.remove(downScope.size()-1);
    return b;
  }

  /**
  *  Tries to use an Negation Elimination to get p
  */  
  private boolean NE(WFF p, WFF q, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed) {
    if (q.getRightWFF().isLetter()) { return false; }
    WFF f;
    while (q.getRightWFF().getMainLogicalParticle().getParticle().equals("~")) {
      System.out.println("use NE on "+q.getFormula());
      f = q;
      q = q.getRightWFF().getRightWFF();
      downScope.add(q);  
      want(f, hyp, new ArrayList(downScope), NIAssumed);
      hyp.add(new NENode(m_LineNum,q,m_LineNum - 1));
      m_LineNum++;
      if (p.equals(q)) { return true; }
      if ((q.isLetter()) || (q.getRightWFF().isLetter()) || (!q.getMainLogicalParticle().getParticle().equals("~"))) {
        return false;
      }
      
    }
    return false;
  }

  /**
  *  Tries to use Negation Introduction to get p
  */  
  private boolean NI(WFF p, ArrayList hyp, ArrayList downScope, ArrayList NIAssumed) {
    ArrayList newhyp = new ArrayList();
    
    //System.out.println("try NI:assume:"+p.getRightWFF().getFormula());
    System.out.println("try NI:assume opposite of:"+p.getFormula());
    boolean b = false;
    int saveLineNum = m_LineNum;
    int saveContraNum = 0;
    m_LineNum++;    
    downScope.add(p.getRightWFF());
    WFF w;
    WFF q;
    LogicalParticle l;

    Iterator k = NIAssumed.iterator();
    while (k.hasNext()) { //only want unique NI assumptions
      w = (WFF)k.next();    
      if (w.equals(p.getRightWFF())) { return false; }
    }
    NIAssumed.add(p.getRightWFF());
//System.out.println("here0");
    ArrayList a = new ArrayList();
    Set s = new HashSet();
    Iterator j = downScope.iterator();
    while (j.hasNext()) { //only want unique WFF when looking for contradictions
      w = (WFF)j.next();
      a = addToSet(s, w, a);
    }
//System.out.println("here1");
    Iterator i = a.iterator();
    while (i.hasNext()) {

      q = (WFF)i.next();
      
      l = new Negation();
      w = new WFF(l, null, q);
      
      if (want(q, newhyp, new ArrayList(downScope),NIAssumed)) { 
        saveContraNum = m_LineNum - 1;
        
        if (want(w, newhyp, new ArrayList(downScope),NIAssumed)) {
          System.out.println("got a contradiction with "+q.getFormula()+" and "+w.getFormula());
          b = true;        
          break;
        }
      }
      
    }
    downScope.remove(downScope.size()-1);
    NIAssumed.remove(NIAssumed.size()-1);
    if (b) {
      downScope.add(p);
      
      hyp.add(new HypNode(saveLineNum,p.getRightWFF(), newhyp));      
      
      hyp.add(new NINode(m_LineNum,p, saveLineNum,saveContraNum,m_LineNum-1));
      m_LineNum++;
    } else { 
      m_LineNum--;
      System.out.println("Unable to find a contradiction when "+p.getRightWFF().getFormula()+" was assumed");
    }
    return b;
  }

  /**
  * used by NI to get unique WFFs for finding contradictions
  */
  private ArrayList addToSet(Set s, WFF w, ArrayList a) {
    String str = w.getFormula();
    if ((!(str.substring(0).equals("~"))) && (!s.contains(str))) {
      s.add(str);
      a.add(w);
    } 
    if (!w.isLetter()) {
      
      if (w.getRightWFF() != null) { a = addToSet(s,w.getRightWFF(),a); }
      if ((!w.getMainLogicalParticle().getParticle().equals(">")) && (w.getLeftWFF() != null)) { a = addToSet(s,w.getLeftWFF(),a); }       
    }
    return a;    
  }  
}