package WFFSupport;

import java.util.*;

public class ProofGenerator {

  public ProofGenerator() {}

  public boolean want(WFF p, ArrayList hyp, ArrayList downScope) {
    System.out.println("Want:"+p.getFormula());
    WFF q;    
   //check if you could reiterate p from the downScope
    Iterator i = downScope.iterator();
    while (i.hasNext()) {
      q = (WFF)i.next();
      if (p.equals(q)) {
        System.out.println(q.getFormula()+" is on the current scope line or can be reiterated from a down scope line");
        return true;
      }
    }
    //Smaller Part of Something?
    ArrayList a = new ArrayList(downScope);
    Iterator j = a.iterator();
    while (j.hasNext()) {

      q = (WFF)j.next();
      if ((!q.isLetter()) && (q.contains(p))) {

        //HorseShoe
        if (q.getMainLogicalParticle().getParticle().equals(">")) {
          boolean bool = false;  

          while (q.getRightWFF().contains(p)) {
            if (q.getLeftWFF().equals(p)) { break; }
            bool =  want(q.getLeftWFF(), new ArrayList(hyp), new ArrayList(downScope));
            if (!bool) { return false; }
            else { 
              System.out.println("use M.P. on " + q.getFormula() + " and "+ q.getLeftWFF().getFormula());
              downScope.add(q.getRightWFF()); 

              if (!q.getRightWFF().isLetter()) {

                q = q.getRightWFF();
              } else { 

                return true;
              }

            }
          }
          
        }
      }
    }
System.out.println("Intro Rules?");    
    //Intro Rules?
    if (!p.isLetter()) {
      if (p.getMainLogicalParticle().getParticle().equals(">")) {

        hyp.add(p.getLeftWFF());
        downScope.add(p.getLeftWFF());
        System.out.println("Assume:"+p.getLeftWFF().getFormula());
        boolean b = want(p.getRightWFF(), new ArrayList(hyp), new ArrayList(downScope));
        hyp.remove(hyp.size()-1);
        downScope.remove(downScope.size()-1);
        return b;
      }
    }
    return false;
  }
  
}